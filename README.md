# clientapp-spaservices-extensions

This project demonstrates how to use a plain webpack app (as opposed to a create-react-app/Angular CLI app) in ASP.NET Core.

Here you can see how to do this using `Microsoft.AspNetCore.SpaServices.Extensions`, which is the supported way of doing this. You can compare this with the old way (WebpackDevMiddleware, see https://gitlab.com/raphaelr/clientapp-webpack-dev-middleware).

For more details, see https://tapesoftware.net/aspnet-clientapp/.
