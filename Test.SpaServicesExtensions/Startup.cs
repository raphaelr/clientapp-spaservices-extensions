using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Test.SpaServicesExtensions
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages()
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();
            app.UseRouting();
            app.UseEndpoints(endpoints => endpoints.MapRazorPages());

            if (env.IsDevelopment())
            {
                // If the path starts with "/dist", let webpack handle the request
                // We can't use .Map() here, because that would move the "/dist" from Request.Path to Request.PathBase, which
                // does not work since UseReactDevelopmentServer() only forwards the Path, not the PathBase (which makes sense).
                app.MapWhen(ctx => ctx.Request.Path.StartsWithSegments("/dist"), clientApp =>
                {
                    clientApp.UseSpa(spa =>
                    {
                        spa.Options.SourcePath = Path.Combine(env.ContentRootPath, "ClientApp");

                        // We're not actually using React, but as long as our server uses the "PORT" environment variable
                        // and says the magic words "Starting the development server", it will work just fine.
                        spa.UseReactDevelopmentServer("serve:aspnet");
                    });
                });
            }
        }
    }
}
