const webpack = require("webpack");
const webpackDevMiddleware = require("webpack-dev-middleware");
const express = require("express");

// Set to false if you don't want to use HMR.
const useHotModuleReplacement = true;
const hmrPath = "/dist/__webpack_hmr";

// Set by ReactDevelopmentServerMiddleware
const port = Number(process.env.PORT);

// Load the webpack config
let config = require("./webpack.config");
if (typeof (config) === "function") {
    config = config(/* env */ {}, /* argv */ {});
}

// Add the hot module replacement client
if (useHotModuleReplacement) {
    const modifiedEntries = { };
    for (const entry of Object.entries(config.entry)) {
        let key = entry[0];
        let value = entry[1];

        if (typeof(value) === "string") {
            value = [value];
        }
        value.unshift(`webpack-hot-middleware/client?path=${hmrPath}`);

        modifiedEntries[key] = value;
    };

    config.entry = modifiedEntries;

    if (config.plugins == null) {
        config.plugins = [];
    }
    config.plugins.push(new webpack.HotModuleReplacementPlugin());
}

const compiler = webpack(config);

// ReactDevelopmentServerMiddleware expects this message, otherwise it will
// think we crashed
const aspnetMagic = "Starting the development server";
console.log(`${aspnetMagic} at port ${port}.`);

const app = express();
app.use(webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath,
    headers: {
        "Cache-Control": "no-cache"
    }
}));

if (useHotModuleReplacement) {
    const webpackHotMiddleware = require("webpack-hot-middleware");
    app.use(webpackHotMiddleware(compiler, {
        path: hmrPath
    }));
}
app.listen(port);
