const path = require("path");

// This is the simplest webpack config for what WebpackDevMiddleware expects
// yours will probably be much more complicated

module.exports = function(env) {
    return {
        context: path.resolve(__dirname),

        entry: {
            index: "./src/index"
        },

        output: {
            path: path.resolve(__dirname, "../wwwroot/dist"),
            publicPath: "/dist/"
        },

        mode: "development"
    };
};
